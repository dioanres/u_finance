<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function tanggal_lengkap($date)

{
    $tgl = date('d', strtotime($date));
    $bulan = date('M', strtotime($date));
    $tahun = date('Y', strtotime($date));

    switch ($bulan) {
        case 'Jan':
            $bulan = "Januari";
            //break;
        case 'Feb':
            $bulan = "Februari";
            break;
        case 'Mar':
            $bulan = "Maret";
            break;
        case 'Apr':
            $bulan = "April";
            break;
        case 'Mei':
            $bulan = "Mei";
            break;
        case 'Jun':
            $bulan = "Juni";
            break;
        case 'Jul':
            $bulan = "Juli";
            break;
        case 'Aug':
            $bulan = "Agustus";
            break;
        case 'Sep':
            $bulan = "September";
            break;
        case 'Okt':
            $bulan = "Oktober";
            break;
        case 'Nov':
            $bulan = "November";
            break;
        case 'Dec':
            $bulan = "Desember";
            break;
    }
    return $tgl . ' ' . $bulan . ' ' . $tahun;

}

?>