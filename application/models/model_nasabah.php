<?php 
   class model_nasabah extends CI_Model {
      function __construct() { 
         parent::__construct(); 
      } 

      public function get_data_nasabah(){
        $query = $this->db->get('nasabah')->result();

        return $query; 
      }
      
      public function get_mobil_by_id($id){
         $mobil = $this->db->get_where('mobil', array('id' => $id))->row();
        
         return $mobil;
      }

      
   }

   ?>