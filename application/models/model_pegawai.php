<?php 
   class model_pegawai extends CI_Model {
      function __construct() { 
         parent::__construct(); 
      } 
   
      public function list_pegawai($data){
        $this->db->insert('pegawai', $data);
      }

      public function get_data_sales(){

         $sql = "SELECT * FROM pegawai WHERE `role` = 2";
         $query = $this->db->query($sql)->result();

         return $query;
         
      }

      public function get_pegawai_by_id($id)
      {
         $pegawai = $this->db->get_where('pegawai', array('id' => $id))->row();

         return $pegawai;
      }
   }