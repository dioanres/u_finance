<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    
                <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?php //if ($this->session->userdata('user')->role != '2') {
                    ?>
                      <a href="<?php echo base_url(); ?>index.php/nasabah/tambah"><button type="button" class="btn btn-primary">Tambah</button></a>  
                    <?php 
                //} ?>
                    <?php if ($this->session->flashdata('success')) {
                        ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong>Berhasil !!</strong> <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <?php 
                } ?>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Nama Nasabah</th>
                          <th>Alamat</th>
                          <th>No HP</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

                    </div>
                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                       
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>

</html>
<script type="text/javascript">
    var tabel_member = $('#datatable').DataTable({
        "ajax":"<?= base_url() ?>index.php/nasabah/list_data",
        "dataSrc":"data",
        "info":false,
        "responsive":true,
        "columns": [
            { "data": "nama" },
            { "data": "alamat" },
            { "data": "no_telpon"},
            { "data": null}
            ],
        "columnDefs" : [
            {
            "targets": [3],
            "data":null,
            "defaultContent": "<a class='green' href='#' data-toggle='modal' data-target='.bs-example-modal-lg'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-nasabah'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-nasabah'></i></a>"
            },
        ]
    });


var arr_data_member =[];
$('#datatable tbody').on('click', '.dtl-nasabah, .delete-nasabah', function() {
        $(this).addClass('selected');
        if (typeof tabel_member.row($(this).closest('tr')).data() !== 'undefined') {
            arr_data_member = tabel_member.row($(this).closest('tr')).data();
        } else if (typeof tabel_member.row(this).data() !== 'undefined') {
            arr_data_member = tabel_member.row(this).data();
        } else {
            arr_data_member = tabel_member.columns(  ).row($(this).closest('tr')).data();
        }
        if ($(this).attr('class').includes('dtl-nasabah')) {
          window.location.href = '<?= base_url() ?>'+'index.php/nasabah/edit/'+arr_data_member.id;
        } else {
            swal({
              title: "Apakah Anda Yakin ingin menghapus data ini ?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                console.log(122);
                delete_member(arr_data_member.id);
              }
            });
        }
});

function delete_member(id) {
    $.ajax({
        method:"POST",
        url:"<?= base_url() ?>" + "index.php/nasabah/delete",
        data:{ 'id' : id},
        cache:false,
        success:function(response) {
            if (response=='true') {
                window.location.href='<?= base_url() ?>'+'index.php/nasabah';
            } else {
                alert (response);
            }
        },
        error: function(response) {
             console.log(response);
        }
    });    
}
</script>