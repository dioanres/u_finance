<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header');?>
    <style>
     .error {
       color : red;
     }
     #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
     </style>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar');?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">

                    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="form-siswa" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo base_url(); ?>index.php/nasabah/simpan">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Nasabah <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" name="nama" class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('nama'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <textarea name="alamat" class="form-control col-md-7 col-xs-12"> </textarea>
                          <!-- <input type="text" id="" name="alamat" class="form-control col-md-7 col-xs-12"> -->
                          <div class="error"> <?php echo form_error('alamat'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No Hp<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" id="no_hp" name="no_hp"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('no_hp'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No KTP<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="no-ktp" name="no_ktp"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('no_ktp'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No PBB<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="no-pbb" name="no_pbb"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('no_pbb'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Rekening Koran<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="rek-koran" name="rek_koran"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('rek_koran'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No NPWP<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="no-npwp" name="no_npwp"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('no_npwp'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Slip Gaji<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="slip-gaji" name="slip_gaji"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('slip_gaji'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">SKU<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="sku" name="sku"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('sku'); ?> </div>
                        </div>
                      </div>
                      <?php if ($this->session->flashdata('error')) {?>
                      <div class="col-lg-3"></div>
                      <div class="col-md-6">
                      <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>Gagal !!</strong> <?php echo $this->session->flashdata('error') ?>
                      </div>
                      </div>
                      <?php
}?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="<?php echo base_url() ?>index.php/nasabah"><button class="btn btn-primary" type="button">Kembali</button></a>
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" id="btn-simpan-user" class="btn btn-success">Simpan</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

                <br />
                </div>
                <!-- /page content -->
            </div>
        </div>

        <?php $this->load->view('main/footer');?>

    </body>
</html>
<script>
    // Initialize and add the map
// function initMap() {
//   // The location of Uluru
//   var uluru = {lat: -6.21462, lng: 106.84513};
//   // The map, centered at Uluru
//   var map = new google.maps.Map(
//       document.getElementById('map'), {zoom: 4, center: uluru});
//   // The marker, positioned at Uluru
//   var marker = new google.maps.Marker({position: uluru, map: map});
// }



</script>
