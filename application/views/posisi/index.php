<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">

                <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Posisi</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <a href="<?php echo base_url(); ?>index.php/posisi/tambah"><button type="button" class="btn btn-primary">Tambah</button></a>
                    <?php if ($this->session->flashdata('success')) {
                        ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong>Berhasil !!</strong> <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <?php 
                } ?>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Posisi/Jabatan</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>


                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

                    </div>
                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">

                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
  </div>
</html>
<script type="text/javascript">
    var tabel_pengguna = $('#datatable').DataTable({
        "ajax":"<?= base_url() ?>index.php/posisi/list_data",
        "dataSrc":"data",
        "info":false,
        "responsive":true,
        "columns": [
            {"data":"id"},
            {"data":"nama_posisi"},
            { "data": null}
            ],
        "columnDefs" : [
            {
            "targets": [2],
            "data":null,
            "defaultContent": "<a class='green' href='#' data-toggle='modal' data-target='.bs-example-modal-lg'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-siswa'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-siswa'></i></a>"
            },
            {
              "targets" : [0],
              render: function (data, type, row, meta) {
                  return meta.row + meta.settings._iDisplayStart + 1;
              }
            }
        ]
    });


var arr_data_pengguna =[];
$('#datatable tbody').on('click', '.dtl-siswa, .delete-siswa', function() {
        $(this).addClass('selected');
        if (typeof tabel_pengguna.row($(this).closest('tr')).data() !== 'undefined') {
            arr_data_pengguna = tabel_pengguna.row($(this).closest('tr')).data();
        } else if (typeof tabel_pengguna.row(this).data() !== 'undefined') {
            arr_data_pengguna = tabel_pengguna.row(this).data();
        } else {
            arr_data_pengguna = tabel_pengguna.columns(  ).row($(this).closest('tr')).data();
        }
        if ($(this).attr('class').includes('dtl-siswa')) {
          window.location.href = '<?= base_url() ?>'+'index.php/posisi/edit/'+arr_data_pengguna.id;
        } else {
            swal({
              title: "Apakah Anda Yakin ingin menghapus data ini ?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                console.log(122);
                delete_posisi(arr_data_pengguna.id);
              }
            });
        }
});

function delete_posisi(id) {
    $.ajax({
        method:"POST",
        url:"<?= base_url() ?>" + "index.php/posisi/delete",
        data:{ 'id' : id},
        cache:false,
        success:function(response) {
            console.log(response);
            if (response=='true') {
                //alert('Berhasil Menghapus Data');
                window.location.href='<?= base_url() ?>'+'index.php/posisi';
            } else {
                console.log(response);
                alert (response);
            }
          
        },
        error: function(response) {
             console.log(response);
            //alert(response);
            
        }
    });    
}
</script>