<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <style>
        .media .newdate {
            background: #2dbb2b;
            width: 52px;
            margin-right: 10px;
            border-radius: 10px;
            padding: 5px;
        }
        .values {
          font-size: 30px;
          /* color: black; */
        }

        .judul {
          font-size: 20px;
        }
    </style>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="col-md-12">
                    <center><img src="<?= base_url() ?>assets/img/icon/logo_u_finance.png"></center>
                        
                        <h1><center>Selamat Datang Di Sistem</center></h1>
                        <h1><center>Absensi dan Nasabah </center> </h1> 
                        <h1><center>PT. U Finance Indonesia</center></h1>
                    </div>
                    <div class="row">
              
              
            </div>
                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">

                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    
  </div>
</html>
<script>

function bayar_lunas() {
    var id = $('#id').val();
    //var sisa = $('#sisa').val();
    $.ajax({
        method:"POST",
        url:"<?= base_url() ?>" + "index.php/transaksi/bayar_lunas",
        data:{ 'id' : id},
        cache:false,
        success:function(response) {
            if (response=='true') {
                window.location.href='<?= base_url() ?>'+'index.php/controller_home';
            } else {
                alert (response);
            }
        },
        error: function(response) {
             console.log(response);
        }
    });    
}

function get_harga(id) {
  $.ajax({
      method:"POST",
      url:"<?= base_url() ?>" + "index.php/transaksi/get_total_harga",
      data:{ 'id' : id},
      cache:false,
      success:function(response) {
        var data = $.parseJSON(response);
        console.log(data);
        $('#id').val(id);
        $('#total_harga').val(data.total);
        $('#dp').val(data.dp);
        $('#sisa').val(data.sisa);
        $('#kembalian').val(0);
        $('#bayar').val(0);
      },
      error: function(response) {
            console.log(response);
      }
  });    
}
</script>
