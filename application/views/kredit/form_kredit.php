<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">

                    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Kredit</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />

                    <form id="form-siswa" data-parsley-validate class="form-horizontal form-label-left" method='post' action='<?php echo base_url() ?>index.php/kredit/simpan_kredit' >
                      <input type="hidden" id="id_survey" name="id_survey" value="<?php echo @$data_survey->id; ?>" class="form-control col-md-7 col-xs-12">
                      <input type="hidden" id="id_kredit" name="id_kredit" value="<?php echo @$id_kredit; ?>" class="form-control col-md-7 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nik">Total DP<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" id="total_dp" required="required" name="total_dp" value="<?php echo @$data_kredit->total_dp; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Angsuran<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" id="angsuran" required="required" name="angsuran" value="<?php echo @$data_kredit->angsuran; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Lama Kredit<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" id="lama_kredit" required="required" name="lama_kredit" value="<?php echo @$data_kredit->lama_kredit; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Status Kredit<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select id="role" readonly name="status_kredit" class="select2_single form-control" tabindex="-1" data-live-search="true">
                            <option value="0">Belum Lunas</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Mobil<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="type" id="mobil" required="required" readonly name="mobil" value="<?php echo @$mobil->jenis_mobil; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Sales<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="type" id="sales" required="required" readonly name="sales" value="<?php echo @$sales->nama; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Kepala Cabang yang Approved<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="type" id="kacab" required="required" readonly name="kacab" value="<?php echo @$kacab->nama; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <?php if ($this->session->flashdata('error')) { ?>
                      <div class="col-lg-3"></div>
                      <div class="col-md-6">
                      <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>Gagal !!</strong> <?php echo $this->session->flashdata('error') ?>
                      </div>
                      </div>
                      <?php 
                    } ?>
                      <!-- <div class="ln_solid"></div> -->
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="<?php echo base_url() ?>index.php/survey"><button class="btn btn-primary" type="button">Cancel</button></a> 
                          <button class="btn btn-primary reset" type="button">Reset</button>
                          <button type="submit" id="btn-simpan-siswa" class="btn btn-success">Simpan</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">

                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
</html>
<script type="text/javascript">
$('.reset').click(function(){
        $('#form-siswa').find('input[type=text], input[type=time],select, input[type=number]').val('');
    });

</script>
