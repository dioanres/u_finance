<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <style>
     .error {
       color : red;
     }
     </style>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <?php /* var_dump($listJadwal);
                  exit();  */ ?>
                  <div class="x_content">
                    <br />
                    <form id="form-siswa" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo base_url(); ?>index.php/transaksi/simpan">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Member <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="member" class="select2_single form-control" tabindex="-1" data-live-search="true">
                              <option value="">Pilih Member</option>
                              <?php foreach ($member as $key => $value) { ?>
                                  <option value="<?php echo $value->id; ?>" <?php if ($value->id == $data->id_member) {
                                                                              echo "selected";
                                                                            } ?>><?php echo $value->nama_member; ?></option>
                            <?php 

                          } ?>
                          </select>
                          <div class="error"> <?php echo form_error('member'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Main<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="tanggal_main" id="tanggal_main" value="<?php echo date('m/d/Y', strtotime($data->tanggal_main)); ?>" type="text" class="form-control has-feedback-left single_cal1" id="" placeholder="First Name" aria-describedby="inputSuccess2Status">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status" class="sr-only">(success)</span>
                          <div class="error"> <?php echo form_error('tanggal_main'); ?> </div>
                        </div>
                        <div> <button type="button" id="lihat" name='lihat' class="btn btn-success">Lihat Jadwal</button></div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Jadwal Lapangan A</h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                            <div class="">
                              <ul class="to_do" id="lapangan-a">
                                
                              </ul>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Jadwal Lapaangan B </h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                            <div class="">
                              <ul class="to_do" id="lapangan-b">
                               
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <center><div class="error"> <?php echo form_error('jadwal'); ?> </div></center>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Bayar DP<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="bayar_dp" name="bayar_dp" class="form-control col-md-7 col-xs-12 uang" value="">
                          <div class="error"> <?php echo form_error('bayar_dp'); ?> </div>
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status Pembayaran<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="status-pembayaran" name="status_pembayaran" class="select2_single form-control" tabindex="-1" data-live-search="true">
                            <option value="">Pilih Status Pembayaran</option>
                            <option value="0">Belum Bayar</option>
                            <option value="1">Bayar DP</option>
                            <option value="2">Lunas</option>
                          </select>
                          <div class="error"> <?php echo form_error('total_harga'); ?> </div>
                        </div>
                      </div>
                      <?php if ($this->session->flashdata('error')) { ?>
                      <div class="col-lg-3"></div>
                      <div class="col-md-6">
                      <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>Gagal !!</strong> <?php echo $this->session->flashdata('error') ?>
                      </div>
                      </div>
                      <?php 
                    } ?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12">
                          <div class="col-md-4"></div>
                          <div class="col-md-4">
                            <center>
                            <a href="<?php echo base_url() ?>index.php/transaksi"><button class="btn btn-primary" type="button">Cancel</button></a> 
                            <button class="btn btn-primary reset" type="button">Reset</button>
                            <button type="submit" name="simpan" id="btn-simpan-user" class="btn btn-success">Simpan</button>
                            </center>
                          </div>
                          <div class="col-md-4"></div>
                        </div>
                       
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

                <br />
                </div>
                <!-- /page content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
</html>

<script>
    $('.reset').click(function(){
        $('#form-siswa').find('input[type=text], input[type=time],select').val('');
    });
    
    $(document).ready(function(){
      list_jadwal();
      $( '#bayar_dp' ).mask('000.000.000', {reverse: true});
    });

    $('#lihat').click(function(){
      list_jadwal();
    });
    
    function list_jadwal() {
      let tanggal_main = $('#tanggal_main').val();
      $.ajax({
          method:"GET",
          url:"<?= base_url() ?>" + "index.php/transaksi/list_jadwal_edit/"+<?= $data->id; ?>,
          //data:{'id' : tanggal_main},
          cache:false,
          success:function(response) {
              $('#lapangan-a').empty();
              $('#lapangan-b').empty();
              var data = $.parseJSON(response);
              var listJadwal = <?= $listJadwal ?>;
              console.log(listJadwal);
              $.each(data.lapanganA,function(i,v){
              var ada = listJadwal.indexOf(v.id);
              var cek = '';
                if (ada >= 0) {
                  cek = 'checked';
                }
                $('#lapangan-a').append('<li><p><input type="checkbox" '+cek+' value="'+v.id+'" data-harga="'+v.harga+'" name="jadwal[]" class="flat jadwal icheckbox_flat-green"> Jam : <b> '+v.jam_mulai+' s/d '+v.jam_selesai+' </b> Harga : <b>'+v.harga+'</b> </p></li>');
              });

              $.each(data.lapanganB,function(i,v){
                var cek = '';
                var ada = listJadwal.indexOf(v.id);
                if (ada >= 0) {
                  cek = 'checked';
                }
                $('#lapangan-b').append('<li><p><input type="checkbox" '+cek+' value="'+v.id+'" data-harga="'+v.harga+'" name="jadwal[]" class="flat jadwal icheckbox_flat-green"> Jam : <b> '+v.jam_mulai+' s/d '+v.jam_selesai+' </b> Harga : <b>'+v.harga+'</b> </p></li>');
              });
          },
          error: function(response) {
                  console.log(response);
          }
      }); 
    }

    $('#waktu-main,#waktu-selesai,#lapangan').keyup(function(){
        var awal = parseInt($('#waktu-main').val());
        var akhir = parseInt($('#waktu-selesai').val());
        var total = akhir - awal ;
        if (isNaN(total)) {
            total = 0 ;
        }
        $('#total-jam').val(total);
        get_harga();
    });

    $('#waktu-main,#waktu-selesai,#lapangan').change(function(){
        var awal = parseInt($('#waktu-main').val());
        var akhir = parseInt($('#waktu-selesai').val());
        var total = akhir - awal ;
        if (isNaN(total)) {
            total = 0 ;
        }
        $('#total-jam').val(total);
        get_harga();
    });

    $('#total-harga,#bayar,#waktu-selesai').change(function(){
      var sisa = parseInt($('#total-harga').val())- parseInt($('#bayar').val());
      $('#sisa').val(sisa);
    });

    $('#total-harga,#bayar').blur(function(){
      var sisa = parseInt($('#total-harga').val())- parseInt($('#bayar').val());
      $('#sisa').val(sisa);
    });

    function get_harga() {
        $.ajax({
            method:"POST",
            url:"<?= base_url() ?>" + "index.php/transaksi/get_harga_lapangan",
            data:{'id' : $('#lapangan').val()},
            cache:false,
            success:function(response) {
                var data = $.parseJSON(response);
                var total_harga = data.harga * parseInt($('#total-jam').val());
                $('#total-harga').val(total_harga);
                var sisa = parseInt($('#total-harga').val())- parseInt($('#bayar').val());
                $('#sisa').val(sisa);
            },
            error: function(response) {
                    console.log(response);
                //alert(response);
            }
        }); 
    }
       
</script>
