<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <style>
     .error {
       color : red;
     }
     </style>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    
                    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  
                  <div class="x_content">
                    <br />
                    <form id="form-siswa" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo base_url(); ?>index.php/transaksi/simpan">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Member <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="member" class="select2_single form-control" tabindex="-1" data-live-search="true">
                              <option value="">Pilih Member</option>
                              <?php foreach ($member as $key => $value) { ?>
                                  <option value="<?php echo $value->id; ?>"><?php echo $value->nama_member; ?></option>
                            <?php 
                          } ?>
                          </select>
                          <div class="error"> <?php echo form_error('member'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Lapangan</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select id="lapangan" name="lapangan" class="select2_single form-control" tabindex="-1" data-live-search="true">
                              <option value="">Pilih Lapangan</option>
                              <?php foreach ($lapangan as $key => $value) { ?>
                                  <option value="<?php echo $value->id_lapangan; ?>"><?php echo $value->nama_lapangan; ?></option>
                            <?php 
                          } ?>
                          </select>
                          <div class="error"> <?php echo form_error('lapangan'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Main<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="tanggal_main" type="text" class="form-control has-feedback-left single_cal1" id="" placeholder="First Name" aria-describedby="inputSuccess2Status">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status" class="sr-only">(success)</span>
                          <div class="error"> <?php echo form_error('tanggal_main'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Waktu Main<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="time" id="waktu-main" name="waktu_main" class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('waktu_main'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Waktu Selesai<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="time" id="waktu-selesai" name="waktu_selesai" class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('waktu_selesai'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Total Jam<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="total-jam" name="total_jam" class="form-control col-md-7 col-xs-12" readonly>
                          <div class="error"> <?php echo form_error('total_jam'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Total Harga<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="total-harga" name="total_harga" class="form-control col-md-7 col-xs-12" readonly>
                          <div class="error"> <?php echo form_error('total_harga'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Bayar<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" id="bayar" name="bayar" class="form-control col-md-7 col-xs-12" value="0">
                          <div class="error"> <?php echo form_error('bayar'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Sisa<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" id="sisa" name="sisa" class="form-control col-md-7 col-xs-12" value="" readonly>
                          <div class="error"> <?php echo form_error('sisa'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status Pembayaran<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="status-pembayaran" name="status_pembayaran" class="select2_single form-control" tabindex="-1" data-live-search="true">
                            <option value="">Pilih Status Pembayaran</option>
                            <option value="0">Belum Bayar</option>
                            <option value="1">Bayar DP</option>
                            <option value="2">Lunas</option>
                          </select>
                          <div class="error"> <?php echo form_error('total_harga'); ?> </div>
                        </div>
                      </div>
                      <?php if ($this->session->flashdata('error')) { ?>
                      <div class="col-lg-3"></div>
                      <div class="col-md-6">
                      <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>Gagal !!</strong> <?php echo $this->session->flashdata('error') ?>
                      </div>
                      </div>
                      <?php 
                    } ?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="<?php echo base_url() ?>index.php/transaksi"><button class="btn btn-primary" type="button">Cancel</button></a> 
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" id="btn-simpan-user" class="btn btn-success">Simpan</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

                <br />
                </div>
                <!-- /page content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
</html>

<script>
    $('#waktu-main,#waktu-selesai,#lapangan').keyup(function(){
        var awal = parseInt($('#waktu-main').val());
        var akhir = parseInt($('#waktu-selesai').val());
        var total = akhir - awal ;
        if (isNaN(total)) {
            total = 0 ;
        }
        $('#total-jam').val(total);
        get_harga();
    });

    $('#waktu-main,#waktu-selesai,#lapangan').change(function(){
        var awal = parseInt($('#waktu-main').val());
        var akhir = parseInt($('#waktu-selesai').val());
        var total = akhir - awal ;
        if (isNaN(total)) {
            total = 0 ;
        }
        $('#total-jam').val(total);
        get_harga();
    });

    $('#total-harga,#bayar,#waktu-selesai').change(function(){
      var sisa = parseInt($('#total-harga').val())- parseInt($('#bayar').val());
      $('#sisa').val(sisa);
    });

    $('#total-harga,#bayar').blur(function(){
      var sisa = parseInt($('#total-harga').val())- parseInt($('#bayar').val());
      $('#sisa').val(sisa);
    });

    function get_harga() {
        $.ajax({
            method:"POST",
            url:"<?= base_url() ?>" + "index.php/transaksi/get_harga_lapangan",
            data:{'id' : $('#lapangan').val()},
            cache:false,
            success:function(response) {
                var data = $.parseJSON(response);
                var total_harga = data.harga * parseInt($('#total-jam').val());
                $('#total-harga').val(total_harga);
                var sisa = parseInt($('#total-harga').val())- parseInt($('#bayar').val());
                $('#sisa').val(sisa);
            },
            error: function(response) {
                    console.log(response);
                //alert(response);
            }
        }); 
    }
       
</script>
