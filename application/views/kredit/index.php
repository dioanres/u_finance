<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    
                <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?php if ($this->session->flashdata('success')) {
                      ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong>Berhasil !!</strong> <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <?php 
                  } ?>
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nama Nasabah</th>
                          <th>Jenis Kendaraan</th>
                          <th>Total DP</th>
                          <th>Angsuran</th>
                          <th>Lama Kredit</th>
                          <th>Tanggal Pengajuan</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

                    </div>
                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                       
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">Pelunasan Pembayaran</h4>
        </div>
        <div class="modal-body">
          <form id="form-bobot" data-parsley-validate class="form-horizontal form-label-left">
              <input type="text" name="id" id="id" hidden="">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Total Harga<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="total_harga" readonly required="required" name="total_harga" class="form-control col-md-7 col-xs-12">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">DP<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="dp" name="dp" readonly required="required" class="form-control col-md-7 col-xs-12">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Sisa<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="sisa" name="sisa" readonly required="required" class="form-control col-md-7 col-xs-12">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Bayar<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="bayar" name="bayar" required="required" class="form-control col-md-7 col-xs-12 uang">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kembalian<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="kembalian" name="kembalian" readonly required="required" class="form-control col-md-7 col-xs-12 uang">
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 <!--  <button class="btn btn-primary" type="reset">Reset</button> -->
                  <button type="button" id="btn-simpan-bayar" class="btn btn-success">Simpan</button>
                </div>
              </div>
            </form>
        </div>

      </div>
    </div>
  </div>
</html>
<script type="text/javascript">
    var tabel_kredit = $('#datatable').DataTable({
        "ajax":"<?= base_url() ?>index.php/kredit/list_data",
        "dataSrc":"data",
        "info":false,
        "responsive":true,
        "lengthChange": false,
        "ordering":false,
        "columns": [
            { "data": "nama_nasabah" },
            { "data": "jenis_mobil" },
            { "data": "total_dp"},
            { "data" : "angsuran"},
            { "data" : "lama_kredit"},
            { "data" : "tanggal_ajukan"},
            { "data": null}
            ],
        "columnDefs" : [
            {
            "targets": [6],
            "data":null,
            "defaultContent": "<a class='green' href='#' data-toggle='modal' data-target='.bs-example-modal-lg'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-kredit'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-kredit'></i></a>"
            }
        ] 
    });

/* fungsi untuk ubah */

//<a class='red' href='#'><button type='button' class='btn btn-warning btn-xs ubah'>Ubah</button></a>

var arr_data_kredit =[];
$('#datatable tbody').on('click', '.dtl-kredit, .delete-kredit', function() {
        $(this).addClass('selected');
        if (typeof tabel_kredit.row($(this).closest('tr')).data() !== 'undefined') {
            arr_data_kredit = tabel_kredit.row($(this).closest('tr')).data();
        } else if (typeof tabel_kredit.row(this).data() !== 'undefined') {
            arr_data_kredit = tabel_kredit.row(this).data();
        } else {
            arr_data_kredit = tabel_kredit.columns(  ).row($(this).closest('tr')).data();
        }
        if ($(this).attr('class').includes('dtl-kredit')) {
          window.location.href = '/kredit/edit/'+arr_data_kredit.id;
        } else if ($(this).attr('class').includes('bayar')) {
            get_harga(arr_data_kredit.id);
        } else {
            swal({
              title: "Apakah Anda Yakin ingin menghapus data ini ?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                delete_kredit(arr_data_kredit.id);
              }
            });
        }
});

$('#bayar').change(function(){
  $(this).val().replace('.','');
  var sisa = parseInt($('#bayar').val().replace('.',''))  - parseInt($('#sisa').val());
  $('#kembalian').val(sisa);

});

$('#bayar').keyup(function(){
  $(this).val().replace('.','');
  console.log($(this).val());
  var sisa = parseInt($('#bayar').val().replace('.','')) - parseInt($('#sisa').val());
  if (isNaN(sisa)) {
    sisa = 0;
  }
  $('#kembalian').val(sisa);

});

$('#btn-simpan-bayar').click(function(){
  bayar_lunas();
});

function delete_kredit(id) {
    $.ajax({
        method:"POST",
        url:"<?= base_url() ?>" + "index.php/kredit/delete",
        data:{ 'id' : id},
        cache:false,
        success:function(response) {
            if (response=='true') {
                window.location.href='<?= base_url() ?>'+'index.php/kredit';
            } else {
                alert (response);
            }
        },
        error: function(response) {
             console.log(response);
        }
    });    
}

function bayar_lunas() {
    var id = $('#id').val();
    //var sisa = $('#sisa').val();
    $.ajax({
        method:"POST",
        url:"<?= base_url() ?>" + "index.php/transaksi/bayar_lunas",
        data:{ 'id' : id},
        cache:false,
        success:function(response) {
            if (response=='true') {
                window.location.href='<?= base_url() ?>'+'index.php/transaksi';
            } else {
                alert (response);
            }
        },
        error: function(response) {
             console.log(response);
        }
    });    
}

function get_harga(id) {
  $.ajax({
      method:"POST",
      url:"<?= base_url() ?>" + "index.php/transaksi/get_total_harga",
      data:{ 'id' : id},
      cache:false,
      success:function(response) {
        var data = $.parseJSON(response);
        $('#id').val(id);
        $('#total_harga').val(data.total);
        $('#dp').val(data.dp);
        $('#sisa').val(data.sisa);
        $('#kembalian').val(0);
        $('#bayar').val(0);
      },
      error: function(response) {
            console.log(response);
      }
  });    
}
</script>