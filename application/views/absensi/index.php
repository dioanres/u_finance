<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    
                <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!-- hanya untuk admin dan direktur -->
                    <?php if ($this->session->userdata('user')->role != '2') {
                      ?>
                      <a href="<?php echo base_url(); ?>index.php/absensi/tambah"><button type="button" class="btn btn-primary">Tambah</button></a>
                    <?php 
                  } ?>
                    <!-- ================================= -->
                    <?php if ($this->session->flashdata('success')) {
                      ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong>Berhasil !!</strong> <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <?php 
                  } ?>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Tanggal</th>
                          <th>Absen Masuk</th>
                          <th>Absen Pulang</th>
                          <th>Nik Pegawai</th>
                          <th>Nama Pegawai</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

                    </div>
                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                       
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">Form User</h4>
        </div>
        <div class="modal-body">
          <form id="form-siswa" data-parsley-validate class="form-horizontal form-label-left">
              <input type="text" name="id" id="kode_pendaftaran" hidden="">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="nama" name="nama" required="required" class="form-control col-md-7 col-xs-12">
                </div>
              </div>
              <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   <textarea id="alamat" required="required" class="form-control" name="alamat" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."
                    data-parsley-validation-threshold="10"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div id="gender" class="btn-group" data-toggle="buttons">
                    <label id="lbl-l" class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="jk" value="l" id="jk-l"> &nbsp; Laki laki &nbsp;
                    </label>
                    <label id="lbl-w" class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="jk" value="w" id="jk-w"> Wanita
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No Hp <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="no_hp" name="no_hp"  class="form-control col-md-7 col-xs-12">
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="reset">Reset</button>
                  <button type="button" id="btn-simpan-siswa" class="btn btn-success">Submit</button>
                </div>
              </div>
            </form>
        </div>

      </div>
    </div>
  </div>
</html>
<script type="text/javascript">
    var tabel_absen = $('#datatable').DataTable({
        "ajax":"<?= base_url() ?>index.php/absensi/list_data",
        "dataSrc":"data",
        "info":false,
        "responsive":true,
        "columns": [
            { "data": "tanggal" },
            { "data": "absen_msk" },
            { "data": "absen_plg"},
            { "data": "nik"},
            { "data": "nama" },
            { "data": null}
            ],
        "columnDefs" : [
            {
            "targets": [5],
            "data":null,
            "defaultContent": "<a class='green' href='#' ><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-absen'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-absen'></i></a>"
            },
            
        ]
    });


var arr_data_absen =[];
$('#datatable tbody').on('click', '.dtl-absen, .delete-absen', function() {
        $(this).addClass('selected');
        if (typeof tabel_absen.row($(this).closest('tr')).data() !== 'undefined') {
            arr_data_absen = tabel_absen.row($(this).closest('tr')).data();
        } else if (typeof tabel_absen.row(this).data() !== 'undefined') {
            arr_data_absen = tabel_absen.row(this).data();
        } else {
            arr_data_absen = tabel_absen.columns(  ).row($(this).closest('tr')).data();
        }
        if ($(this).attr('class').includes('dtl-absen')) {
          window.location.href = '<?= base_url() ?>'+'index.php/absensi/edit/'+arr_data_absen.id;
        } else {
            swal({
              title: "Apakah Anda Yakin ingin menghapus data ini ?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                console.log(122);
                delete_lapangan(arr_data_absen.id);
              }
            });
        }
});

$('#btn-simpan-siswa').click(function(){
    $.ajax({
            method:"POST",
            url:"<?= base_url() ?>" + "index.php/controller_user/simpan",
            data:$("#form-siswa").serialize(),
            cache:false,
            success:function(response) {
                console.log(response);
                if (response=='true') {
                    alert('Berhasil Menyimpan Data');
                    window.location.href='<?= base_url() ?>'+'index.php/controller_user';
                } else {
                    console.log(response);
                    alert (response);
                }
              
            },
            error: function(response) {
                 console.log(response);
                //alert(response);
                
            }
    });    
});

function delete_lapangan(id) {
    console.log('delete');
    $.ajax({
        method:"POST",
        url:"<?= base_url() ?>" + "index.php/absensi/delete",
        data:{ 'id' : id},
        cache:false,
        success:function(response) {
            console.log(response);
            if (response=='true') {
                //alert('Berhasil Menghapus Data');
                window.location.href='<?= base_url() ?>'+'index.php/absensi';
            } else {
                console.log(response);
                alert (response);
            }
          
        },
        error: function(response) {
             console.log(response);
            //alert(response);
            
        }
    });    
}
</script>