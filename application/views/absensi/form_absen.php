<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('main/header'); ?>
<style>
.error {
    color: red;
}
</style>

<body class="nav-md">

    <div class="container body">
        <div class="main_container">
            <?php $this->load->view('main/side_bar'); ?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Absensi</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br />
                                <form id="form-siswa" data-parsley-validate class="form-horizontal form-label-left"
                                    method="post" action="<?php echo base_url(); ?>index.php/absensi/simpan">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <fieldset>
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <div class="col-md-12 xdisplay_inputx form-group has-feedback" style ="margin-left : -9px;">
                                                            <input type="text" class="form-control has-feedback-left"
                                                                id="single_cal2" placeholder="First Name"
                                                                aria-describedby="inputSuccess2Status2" name="tanggal">
                                                            <span class="fa fa-calendar-o form-control-feedback left"
                                                                aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status2"
                                                                class="sr-only">(success)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <!-- <input type="text" id="tanggal" name="tanggal" class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('tanggal'); ?> </div> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Absen
                                            Masuk</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group bootstrap-timepicker">
                                                <input id="" type="time"
                                                    class="form-control input-small" name="absen_masuk">
                                                <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-time"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Absen
                                            Pulang</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group bootstrap-timepicker">
                                                <input id="" type="time"
                                                    class="form-control input-small" name="absen_pulang"> 
                                                <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-time"></i></span>
                                            </div>
                                            <div class="error"> <?php echo form_error('absen_plg'); ?> </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nik
                                            Pegawai</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="role" name="nik_pegawai" class="select2_single form-control"
                                                tabindex="-1" data-live-search="true">
                                                <option value="">Pilih Pegawai</option>
                                                <?php foreach ($data as $key => $value) {
                                      echo "<option value='".$value->id."'>".$value->nama."</option>";
                                    } ?>
                                            </select>
                                            <div class="error"> <?php echo form_error('nik'); ?> </div>
                                        </div>
                                    </div>

                                    <?php if ($this->session->flashdata('error')) { ?>
                                    <div class="col-lg-3"></div>
                                    <div class="col-md-6">
                                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close"><span aria-hidden="true">×</span>
                                            </button>
                                            <strong>Gagal !!</strong> <?php echo $this->session->flashdata('error') ?>
                                        </div>
                                    </div>
                                    <?php 
                    } ?>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <a href="<?php echo base_url() ?>index.php/absensi"><button
                                                    class="btn btn-primary" type="button">Kembali</button></a>
                                            <button class="btn btn-primary" type="reset">Reset</button>
                                            <button type="submit" id="btn-simpan-user"
                                                class="btn btn-success">Simpan</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <br />
            </div>
            <!-- /page content -->

            <!-- footer content -->
            <footer>
                <div class="pull-right">

                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

    <?php $this->load->view('main/footer'); ?>

</body>

</html>
<script type="text/javascript">
/*  $('#btn-simpan-user').click(function(){
        $.ajax({
            method:"POST",
            url:"<?= base_url() ?>" + "index.php/controller_user/simpan",
            data:$("#form-siswa").serialize(),
            cache:false,
            success:function(response) {
                console.log(response);
                if (response=='true') {
                    alert('Berhasil Menyimpan Data')
                } else {
                    console.log(response);
                    alert (response);
                }
              
            },
            error: function(response) {
                 console.log(response);
                //alert(response);
                
            }
        });    
    }); */
</script>