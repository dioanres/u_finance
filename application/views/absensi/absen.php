<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('main/header'); ?>
<style>
.error {
    color: red;
}
</style>

<body class="nav-md">

    <div class="container body">
        <div class="main_container">
            <?php $this->load->view('main/side_bar'); ?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="x_panel">
                            <div>
                                <center>
                                    <h2>Absen Karyawan</h2>
                                </center>
                                <hr>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <form id="form-absen">
                                    <div class ="col-md-12 col-md-12 col-xs-12">
                                        <center id="absen-pegawai">
                                        <?= $data ?>
                                        </center>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-cm-4 col-xs-12">
                    </div>
                </div>
            </div>
            <!-- /page content -->

            <!-- footer content -->
            <footer>
                <div class="pull-right">

                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

    <?php $this->load->view('main/footer'); ?>

</body>

</html>
<script type="text/javascript">
    var base_url = '<?= base_url(); ?>';
    console.log(base_url);
    $(document).ready(function(){
    });
    $('#btn-hadir').click(function(){
        $.ajax({
            method:"POST",
            url:"<?= base_url() ?>" + "index.php/absensi/absen_pegawai",
            data:$("#form-absen").serialize(),
            cache:false,
            success:function(response) {
                console.log(response);
                if (response=='true') {
                    alert('Terima Kasih')
                    $('#btn-hadir').remove();
                    $('#absen-pegawai').html('<img src = " '+base_url+'assets/img/icon/icon_hadir.png " style="height:130px;">');
                } else {
                    console.log(response);
                    alert (response);
                }
              
            },
            error: function(response) {
                 console.log(response);
                //alert(response);
                
            }
        });    
    });
    $('#btn-pulang').click(function(){
        $.ajax({
            method:"POST",
            url:"<?= base_url() ?>" + "index.php/absensi/proses_absen_pulang",
            data:$("#form-absen").serialize(),
            cache:false,
            success:function(response) {
                console.log(response);
                if (response=='true') {
                    alert('Terima Kasih')
                    $('#btn-hadir').remove();
                    $('#absen-pegawai').html('<img src = " '+base_url+'assets/img/icon/icon_hadir.png " style="height:130px;">');
                } else {
                    console.log(response);
                    alert (response);
                }
              
            },
            error: function(response) {
                 console.log(response);
                //alert(response);
                
            }
        });    
    });  
</script>