<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Aplikasi Topsis </title>

    <?php $this->load->view('main/header'); ?>
  </head>
  
  <body class="login" background ="<?php echo base_url(); ?>assets/img/bg/coin.jpeg">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="post" action="<?php echo base_url(); ?>index.php/controller_login/proses_login">
              <h1>Login Form</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" name="username" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" name="password" />
              </div>
              <div>
                <select class="form-control col-md-7 col-xs-12" id="nilai_bobot" name="as_login" >
                  <option value="">Pilih Login Sebagai</option>
                  <option value="1">Admin</option>
                  <option value="2">Staff</option>
                  <option value="3">Calon Siswa</option>
                </select>
              </div>
              <br/>
              <div class="alert-danger"><?php echo validation_errors(); ?></div>
              <div>
                <br>
                
                <button type="submit" class="btn btn-default">Log in</button>
              </div>

              <div class="clearfix"></div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
