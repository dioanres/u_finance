<?php if ($this->session->userdata('user')) {

    ?>
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"> <span>U Finance Indonesia</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <!-- <div class="profile_pic">
                <img src="<?php echo base_url() ?>assets/images/img.jpg" alt="<?php echo base_url() ?>assets." class="img-circle profile_img">
            </div> -->
            <div class="profile_info">
                <span>Selamat Datang, <h4><?php echo $this->session->userdata('user')->nama; ?></h4></span>
                <h2></h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Menu</h3>
                <ul class="nav side-menu">
                    <li><a href="<?php echo base_url(); ?>index.php/controller_home"><i class="fa fa-home"></i>
                            Dashboard </a>
                    </li>
                    <?php if ($this->session->userdata('user')->role == 1) {?>
                    <li><a href="<?php echo base_url(); ?>index.php/pegawai"><i class="fa fa-user"></i> Data Pegawai
                        </a>
                    </li>
                    
                    <li><a href="<?php echo base_url(); ?>index.php/nasabah"><i class="fa fa-user"></i> Nasabah
                        </a>
                    </li>
                    <li><a href="<?php echo base_url(); ?>index.php/survey"><i class="fa fa-edit"></i> Survey </a>
                    <!-- <li><a href="<?php echo base_url(); ?>index.php/posisi"><i class="fa fa-briefcase"></i> Posisi / Jabatan</a>
                    </li> -->
                    <li><a href="<?php echo base_url(); ?>index.php/mobil"><i class="fa fa-car"></i> Data Mobil </a>
                    </li>

                    <li><a href="<?php echo base_url(); ?>index.php/laporan"><i class="fa fa-book"></i> Laporan </a>
                    </li>

                    <?php
}?>

                    <?php if ($this->session->userdata('user')->role == 2) {?>
                    
                    <li><a href="<?php echo base_url(); ?>index.php/nasabah"><i class="fa fa-user"></i> Nasabah
                        </a>
                    </li>
                    <li><a href="<?php echo base_url(); ?>index.php/survey"><i class="fa fa-edit"></i> Survey </a>
                    <li><a href="<?php echo base_url(); ?>index.php/kredit"><i class="fa fa-credit-card"></i> Kredit </a>
                    <?php
}?>                 
                    <?php if($this->session->userdata('user')->role == 3 ) {
                        ?>
                    <li><a href="<?php echo base_url(); ?>index.php/survey"><i class="fa fa-edit"></i> Verifikasi Survey </a>
                    <?php } ?>
                    
                    </li>
                    
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                        aria-expanded="false">
                        <img src="<?php echo base_url(); ?>assets/img/icon/customer-service.png" alt="">
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">

                        <li><a href="<?php echo base_url(); ?>index.php/controller_login/logout"><i
                                    class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>

<?php
} else {
    redirect(site_url('login'), 'refresh');
}?>