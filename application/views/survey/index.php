<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">

                <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Survey</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <a href="<?php echo base_url(); ?>index.php/survey/tambah"><button type="button" class="btn btn-primary">Tambah</button></a>
                    <?php if ($this->session->flashdata('success')) {
                        ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong>Berhasil !!</strong> <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <?php 
                } ?>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Sales</th>
                          <th>Nama Nasabah</th>
                          <th>Status Survey</th>
                          <th>Status Kredit</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>


                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

                    </div>
                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">

                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
  </div>
</html>
<script type="text/javascript">
    
      var role = <?= $this->session->userdata('user')->role; ?> ;
    console.log(role);
    var aksi = "<a class='green' href='#' data-toggle='modal' data-target='.bs-example-modal-lg'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-survey'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-survey'></i></a><a href='#'><i class='ace-icon fa fa-info bigger-130 col-sm-2 detail-survey'></i></a>";
    var url = "<?= base_url() ?>index.php/survey/list_data";
    if (role == 3) {
      aksi = '<button type="button" class="btn btn-success btn-xs verifikasi">verifikasi</button>';
      url = "<?= base_url() ?>index.php/kredit/list_verifikasi";
    }

    var tabel_survey = $('#datatable').DataTable({
        "ajax":url,
        "dataSrc":"data",
        "info":false,
        "responsive":true,
        "columns": [
            {"data":null},
            {"data":"nama_sales"},
            {"data":"nama_nasabah"},
            {"data" : "status_survey"},
            {"data" : "status_kredit"},
            { "data": null}
            ],
        "columnDefs" : [
            {
            "targets": [5],
            render: function(data,row) {
              
              if (data.status_kredit == 2 || data.status_kredit == -1)
              {
                return "<a href='#'><i class='ace-icon fa fa-info bigger-130 col-sm-2 detail-survey'></i></a>";
              } else {
                return aksi;
              }
            }
            //"data":null,
            //"defaultContent": "<a class='green' href='#' data-toggle='modal' data-target='.bs-example-modal-lg'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-survey'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-survey'></i></a><a href='#'><i class='ace-icon fa fa-info bigger-130 col-sm-2 detail-survey'></i></a>"
            },
            {
              "targets" : [0],
              render: function (data, type, row, meta) {
                  return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            {
                "targets" : [3],
                render: function(data) {
                    return data == 0 ? 'Belum diproses' : 'Sudah Disurvey';
                }
            },
            {
                "targets": 4,
                "render":function(data,row) {
                    
                    if (data == 0) {
                        var kredit = '<button type="button" class="btn btn-round btn-success btn-xs ajukan">Ajukan Kredit</button>';
                    } else if (data == 1) {
                        var kredit = 'Belum Diverifikasi';
                    } else if (data == 2) {
                        var kredit = 'Diterima';
                    } else if (data == -1) {
                      var kredit = 'Ditolak';
                    }
                    
                    return kredit;
                }
            }

        ]
    });


var arr_data_pengguna =[];
$('#datatable tbody').on('click', '.dtl-survey, .delete-survey, .ajukan, .detail-survey, .verifikasi', function() {
        $(this).addClass('selected');
        if (typeof tabel_survey.row($(this).closest('tr')).data() !== 'undefined') {
            arr_data_pengguna = tabel_survey.row($(this).closest('tr')).data();
        } else if (typeof tabel_survey.row(this).data() !== 'undefined') {
            arr_data_pengguna = tabel_survey.row(this).data();
        } else {
            arr_data_pengguna = tabel_survey.columns(  ).row($(this).closest('tr')).data();
        }
        //console.log(arr_data_pengguna);
        if ($(this).attr('class').includes('dtl-survey')) {
          
          window.location.href = '<?= base_url() ?>'+'index.php/survey/edit/'+arr_data_pengguna.id_survey;
        } else if ($(this).attr('class').includes('ajukan')) {
            window.location.href = '<?= base_url() ?>'+'index.php/kredit/ajukan_kredit/'+arr_data_pengguna.id_survey;
        } else if ($(this).attr('class').includes('detail-survey') || $(this).attr('class').includes('verifikasi')) {
          window.location.href = '<?= base_url() ?>'+'index.php/survey/detail/'+arr_data_pengguna.id_survey;
        } 
        else {
            swal({
              title: "Apakah Anda Yakin ingin menghapus data ini ?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                console.log(122);
                delete_posisi(arr_data_pengguna.id_survey);
              }
            });
        }
});

function delete_posisi(id) {
    $.ajax({
        method:"POST",
        url:"<?= base_url() ?>" + "index.php/survey/delete",
        data:{ 'id' : id},
        cache:false,
        success:function(response) {
            console.log(response);
            if (response=='true') {
                //alert('Berhasil Menghapus Data');
                window.location.href='<?= base_url() ?>'+'index.php/survey';
            } else {
                console.log(response);
                alert (response);
            }
          
        },
        error: function(response) {
             console.log(response);
            //alert(response);
            
        }
    });    
}
    
    
</script>