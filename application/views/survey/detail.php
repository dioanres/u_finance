<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">

                    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Data Survey</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <div class = "col-md-12">
                    <form id="form-siswa" data-parsley-validate class="form-horizontal form-label-left" method='post' action='<?php echo base_url() ?>index.php/survey/verifikasi' >
                        <input type ="hidden" name="id" value="<?= $data->id_survey ?>">

                        <span class="section">Info Nasabah</span>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Nasabah<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->nama_nasabah ?>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No Telpon<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->no_telpon ?>
                        </label>
                      </div>
                    
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->alamat ?>
                        </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No KTP<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->no_ktp ?>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No PBB<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->no_pbb ?>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Rek Koran<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->rek_koran ?>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">NPWP<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->npwp ?>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Slip Gaji<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->slip_gaji ?>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">SKU<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->sku ?>
                        </label>
                      </div>

                      <span class="section">Info Kredit</span>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Total DP<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->total_dp ?>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Angsuran<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->angsuran ?>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Lama Kredit<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->lama_kredit ?>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Sales<span class="required">*</span>
                        </label>
                        <label class="control-label" for="last-name">: <?= $data->nama_sales ?>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Verifikasi<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="role" name="status" class="select2_single form-control" tabindex="-1" data-live-search="true">
                            <?php foreach ($status as $key => $value) {
                                if ($this->session->userdata('user')->role == 2) {
                                    if($data->status_kredit == $key) {
                                        echo "<option value='".$key."' selected>".$value."</option>";
                                    }
                                } else {
                                    if($data->status_kredit == $key) {
                                        echo "<option value='".$key."' selected>".$value."</option>";
                                    } else {
                                        echo "<option value='".$key."'>".$value."</option>";
                                    }
                                }
                            } ?>
                          </select>
                        </div>
                      </div>

                      <?php if ($this->session->flashdata('error')) { ?>
                      <div class="col-lg-3"></div>
                      <div class="col-md-6">
                      <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>Gagal !!</strong> <?php echo $this->session->flashdata('error') ?>
                      </div>
                      </div>
                      <?php 
                    } ?>
                      <!-- <div class="ln_solid"></div> -->
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="<?php echo base_url() ?>index.php/survey"><button class="btn btn-primary" type="button">Cancel</button></a> 
                          <?php if($this->session->userdata('user')->role == 3) {
                              ?>
                              <button type="submit" id="btn-simpan-siswa" class="btn btn-success">Simpan</button>
                         <?php } ?>
                          
                        </div>
                      </div>

                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">

                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
</html>
<script type="text/javascript">


</script>
