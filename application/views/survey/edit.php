<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">

                    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Data Survey</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />

                    <form id="form-siswa" data-parsley-validate class="form-horizontal form-label-left" method='post' action='<?php echo base_url() ?>index.php/survey/simpan' >
                      <input type="hidden" id="id" name="id" value="<?php echo @$data->id; ?>" class="form-control col-md-7 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Pegawai/Sales<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="role" name="sales" class="select2_single form-control" tabindex="-1" data-live-search="true">
                            <option value="">Pilih Sales</option>
                            <?php foreach ($sales as $key => $value) {
                                if($data->id_pegawai == $value->id) {
                                    echo "<option value='".$value->id."' selected>".$value->nama."</option>";
                                } else {
                                    echo "<option value='".$value->id."'>".$value->nama."</option>";
                                }
                            } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Nasabah<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="role" name="nasabah" class="select2_single form-control" tabindex="-1" data-live-search="true">
                            <option value="">Pilih Nasabah</option>
                            <?php foreach ($nasabah as $key => $value) {
                              if($data->id_nasabah == $value->id) {
                                echo "<option value='".$value->id."' selected>".$value->nama."</option>";
                              } else {
                                echo "<option value='".$value->id."'>".$value->nama."</option>";
                              }
                              
                            } ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Mobil<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="role" name="mobil" class="select2_single form-control" tabindex="-1" data-live-search="true">
                            <option value="">Pilih Mobil</option>
                            <?php foreach ($mobil as $key => $value) {
                              if($data->id_mobil == $value->id) {
                                echo "<option value='".$value->id."' selected>".$value->jenis_mobil."</option>";
                              } else {
                                echo "<option value='".$value->id."'>".$value->jenis_mobil."</option>";
                              }
                              
                            } ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status Survey<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="role" name="status" class="select2_single form-control" tabindex="-1" data-live-search="true">
                            <option value="">Pilih Status Survey</option>
                            <?php foreach ($status as $key => $value) {
                                if($data->status_survey == $key) {
                                    echo "<option value='".$key."' selected>".$value."</option>";
                                } else {
                                    echo "<option value='".$key."'>".$value."</option>";
                                }
                              
                            } ?>
                          </select>
                        </div>
                      </div>

                      <?php if ($this->session->flashdata('error')) { ?>
                      <div class="col-lg-3"></div>
                      <div class="col-md-6">
                      <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>Gagal !!</strong> <?php echo $this->session->flashdata('error') ?>
                      </div>
                      </div>
                      <?php 
                    } ?>
                      <!-- <div class="ln_solid"></div> -->
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="<?php echo base_url() ?>index.php/survey"><button class="btn btn-primary" type="button">Kembali</button></a> 
                          <button class="btn btn-primary reset" type="button">Reset</button>
                          <button type="submit" id="btn-simpan-siswa" class="btn btn-success">Simpan</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">

                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
</html>
<script type="text/javascript">
$('.reset').click(function(){
    $('#form-siswa').find('input[type=text], input[type=time],select, input[type=number]').val('');
});

</script>
