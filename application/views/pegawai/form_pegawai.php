<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">

                    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Pegawai</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />

                    <form id="form-siswa" data-parsley-validate class="form-horizontal form-label-left" method='post' action='<?php echo base_url() ?>index.php/pegawai/simpan' >
                      <input type="hidden" id="id" name="id" value="<?php echo @$data->id; ?>" class="form-control col-md-7 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nik">Nik<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="nik" required="required" name="nik" value="<?php echo @$data->nik; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="nama" required="required" name="nama" value="<?php echo @$data->nama; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="alamat" required="required" name="alamat" value="<?php echo @$data->alamat; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="email" required="required" name="email" value="<?php echo @$data->email; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telpon">No Telpon<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="no_telpon" required="required" name="no_telpon" value="<?php echo @$data->no_telpon; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Role<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="role" name="posisi" class="select2_single form-control" tabindex="-1" data-live-search="true">
                            <option value="">Pilih Role</option>
                            <?php foreach ($posisi as $key => $value) {
                              echo "<option value='".$value->id."'>".$value->nama_posisi."</option>";
                            } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="password" id="password" name="password" required="required" value="<?php echo @$data->password; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Konfirmasi Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="password" id="konf-password" name="konf_password" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          Aktif:
                          <input type="radio" class="flat" name="status" id="genderM" value="1" checked="" required />
                          Tidak Aktif:
                          <input type="radio" class="flat" name="status" id="genderF" value="0" />
                        </div>
                      </div>

                      <?php if ($this->session->flashdata('error')) { ?>
                      <div class="col-lg-3"></div>
                      <div class="col-md-6">
                      <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>Gagal !!</strong> <?php echo $this->session->flashdata('error') ?>
                      </div>
                      </div>
                      <?php 
                    } ?>
                      <!-- <div class="ln_solid"></div> -->
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="<?php echo base_url() ?>index.php/pegawai"><button class="btn btn-primary" type="button">Cancel</button></a> 
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" id="btn-simpan-siswa" class="btn btn-success">Simpan</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">

                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
</html>
<script type="text/javascript">


</script>
