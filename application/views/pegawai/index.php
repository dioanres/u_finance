<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">

                <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Pegawai</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <a href="<?php echo base_url(); ?>index.php/pegawai/tambah"><button type="button" class="btn btn-primary">Tambah</button></a>
                    <?php if ($this->session->flashdata('success')) {
                        ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong>Berhasil !!</strong> <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <?php 
                } ?>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Nik</th>
                          <th>Nama</th>
                          <th>Posisi</th>
                          <th>Status</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>


                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

                    </div>
                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">

                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
  </div>
</html>
<script type="text/javascript">
    var tabel_pegawai = $('#datatable').DataTable({
        "ajax":"<?= base_url() ?>index.php/pegawai/list_data",
        "dataSrc":"data",
        "info":false,
        "responsive":true,
        "columns": [
            {"data":"nik"},
            {"data":"nama"},
            {"data":"role"},
            { "data": "status_pegawai" },
            { "data": null}
            ],
        "columnDefs" : [
            {
            "targets": [4],
            "data":null,
            "defaultContent": "<a class='green' href='#' data-toggle='modal' data-target='.bs-example-modal-lg'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-pegawai'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-pegawai'></i></a>"
            },
            {
              "targets" : [3],
              "render" : function (data,type,row) {
                return row.status_pegawai == 1 ? '<span class="label label-success">Aktif</span>' : '<span class="label label-danger">Tidak Aktif</span>';
              }
            },
            {
              "targets" : [2],
              "render" : function (data,type,row) {
                return row.role == 1 ? 'Admin' : row.role == 2 ? 'Sales' : row.role == 3 ? 'Direktur' : '';
              }
            }
        ]
    });


var arr_data_pegawai =[];
$('#datatable tbody').on('click', '.dtl-pegawai, .delete-pegawai', function() {
        $(this).addClass('selected');
        if (typeof tabel_pegawai.row($(this).closest('tr')).data() !== 'undefined') {
            arr_data_pegawai = tabel_pegawai.row($(this).closest('tr')).data();
        } else if (typeof tabel_pegawai.row(this).data() !== 'undefined') {
            arr_data_pegawai = tabel_pegawai.row(this).data();
        } else {
            arr_data_pegawai = tabel_pegawai.columns(  ).row($(this).closest('tr')).data();
        }
        if ($(this).attr('class').includes('dtl-pegawai')) {
          window.location.href = '<?= base_url() ?>'+'index.php/pegawai/edit/'+arr_data_pegawai.id;
        } else {

          swal({
              title: "Apakah Anda Yakin ingin menghapus data ini ?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                delete_siswa(arr_data_pegawai.id);
              }
            });
        }
});

function delete_siswa(id) {
    $.ajax({
        method:"POST",
        url:"<?= base_url() ?>" + "index.php/pegawai/delete",
        data:{ 'id' : id},
        cache:false,
        success:function(response) {
            console.log(response);
            if (response=='true') {
                alert('Berhasil Menghapus Data');
                window.location.href='<?= base_url() ?>'+'index.php/pegawai';
            } else {
                console.log(response);
                alert (response);
            }

        },
        error: function(response) {
             console.log(response);
            //alert(response);

        }
    });
}
</script>