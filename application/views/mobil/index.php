<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    
                <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <a href="<?php echo base_url(); ?>index.php/mobil/tambah"><button type="button" class="btn btn-primary">Tambah</button></a>
                    <?php if ($this->session->flashdata('success')) {
                        ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong>Berhasil !!</strong> <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <?php 
                } ?>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Jenis Mobil</th>
                          <th>Warna Mobil</th>
                          <th>Harga Mobil</th>
                          <th>CC Mobil</th>
                          <th>Ketersediaan</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

                    </div>
                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                       
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>

</html>
<script type="text/javascript">
    var tabel_mobil = $('#datatable').DataTable({
        "ajax":"<?= base_url() ?>index.php/mobil/list_data",
        "dataSrc":"data",
        "info":false,
        "responsive":true,
        "columns": [
            { "data": "jenis_mobil" },
            { "data": "warna_mobil" },
            { "data": "harga_mobil"},
            { "data": "cc_mobil"},
            { "data": "ketersediaan"},
            { "data": null}
            ],
        "columnDefs" : [
            {
            "targets": [5],
            "data":null,
            "defaultContent": "<a class='green' href='#' data-toggle='modal' data-target='.bs-example-modal-lg'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-mobil'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-mobil'></i></a>"
            },
        ]
    });


var arr_data_mobil =[];
$('#datatable tbody').on('click', '.dtl-mobil, .delete-mobil', function() {
        $(this).addClass('selected');
        if (typeof tabel_mobil.row($(this).closest('tr')).data() !== 'undefined') {
            arr_data_mobil = tabel_mobil.row($(this).closest('tr')).data();
        } else if (typeof tabel_mobil.row(this).data() !== 'undefined') {
            arr_data_mobil = tabel_mobil.row(this).data();
        } else {
            arr_data_mobil = tabel_mobil.columns(  ).row($(this).closest('tr')).data();
        }
        if ($(this).attr('class').includes('dtl-mobil')) {
          window.location.href = '/mobil/edit/'+arr_data_mobil.id;
        } else {
            swal({
              title: "Apakah Anda Yakin ingin menghapus data ini ?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                console.log(122);
                delete_mobil(arr_data_mobil.id);
              }
            });
        }
});


function delete_mobil(id) {
    $.ajax({
        method:"POST",
        url:"<?= base_url() ?>" + "index.php/mobil/delete",
        data:{ 'id' : id},
        cache:false,
        success:function(response) {
            if (response=='true') {
                window.location.href='<?= base_url() ?>'+'index.php/mobil';
            } else {
                alert (response);
            }
        },
        error: function(response) {
             console.log(response);
        }
    });    
}
</script>