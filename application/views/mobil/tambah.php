<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <style>
     .error {
       color : red;
     }
     </style>
    <body class="nav-md">

        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    
                    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Mobil</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="form-siswa" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo base_url(); ?>index.php/mobil/simpan">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Jenis Barang <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" name="jenis_mobil" class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('jenis_mobil'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Warna Mobil</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" name="warna_mobil" class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('warna_mobil'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Harga<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" id="harga" name="harga_mobil"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('harga_mobil'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Bahan Bakar<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="bahan_bakar" name="bahan_bakar"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error("bahan_bakar"); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">CC<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="cc_mobil" name="cc_mobil"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('cc_mobil'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No Rangka<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="no-rangka" name="no_rangka"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('no_rangka'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No Mesin<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="no_mesin" name="no_mesin"  class="form-control col-md-7 col-xs-12">
                          <div class="error"> <?php echo form_error('no_mesin'); ?> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Stok<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select id="ketersediaan" name="ketersediaan" class="select2_single form-control" tabindex="-1" data-live-search="true">
                            <option value="">-- Stok --</option>
                            <?php foreach ($stok as $key => $value) {
                              echo "<option value='".$key."'>".$value."</option>";
                            } ?>
                          </select>
                          <div class="error"> <?php echo form_error('ketersediaan'); ?> </div>
                        </div>
                      </div>
                      <?php if ($this->session->flashdata('error')) { ?>
                      <div class="col-lg-3"></div>
                      <div class="col-md-6">
                      <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>Gagal !!</strong> <?php echo $this->session->flashdata('error') ?>
                      </div>
                      </div>
                      <?php 
                    } ?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="<?php echo base_url() ?>index.php/mobil"><button class="btn btn-primary" type="button">Kembali</button></a>
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" id="btn-simpan-user" class="btn btn-success">Simpan</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

                <br />
                </div>
                <!-- /page content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>
</html>
