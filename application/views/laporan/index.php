<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('main/header'); ?>
    <style>
        .btn-tampil {
            margin-top: 20px;
            width: 50%;
            margin-bottom: 20px !important; 
        }

        .btn-cetak {
            margin-bottom: 20px !important;
            margin-right: 10px !important;
        }
    </style>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('main/side_bar'); ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                            <div class="x_title">
                                <h2><?php echo $title; ?></h2>
                                <div class="clearfix"></div>
                            </div>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="flat" checked name="pilih" value="all"> Semua Data
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="flat" name="pilih" value="date"> Tanggal
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="col-md-1 col-xs-1">
                                            <label>Dari</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input name="tanggal_main" type="text" class="form-control has-feedback-left single_cal1" id="tanggal-awal" aria-describedby="inputSuccess2Status">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span> 
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <label>Sampai</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input name="tanggal_main" type="text" class="form-control has-feedback-left single_cal1" id="tanggal-akhir" aria-describedby="inputSuccess2Status">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <!-- <form method="get" action="<?php echo base_url(); ?>index.php/laporan/download_semua">
                                                <button type="submit" class="btn btn-round btn-success">Download</button>
                                            </form> -->
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <div class="col-md-12 col-xs-12">
                                        <center>
                                        <button type="button" id="tampil" class="btn btn-primary btn-tampil">Tampilkan</button>
                                        </center>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div id="tabel-data" hidden>
                                    <table id="laporan" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                            <th>Tanggal Pengajuan</th>
                                            <th>Nama Nasabah</th>
                                            <th>Alamat</th>
                                            <th>Jenis Mobil</th>
                                            <th>Total DP</th>
                                            <th>Lama Kredit</th>
                                            <th>Angsuran</th>
                                            <th>Status Kredit</th>
                                            <th>Status Pembayaran</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    <br />
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                       
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <?php $this->load->view('main/footer'); ?>

    </body>

</html>
<script type="text/javascript">

$('#tampil').click(function(){
    
    if ($('input[name=pilih]:checked').val() == 'all') {
        var tabel_member = $('#laporan').DataTable({
                "ajax":"<?= base_url() ?>index.php/laporan/tampil_semua",
                "dataSrc":"data",
                "info":false,
                "responsive":true,
                "destroy": true,
                "searching": false,
                "lengthChange": false,
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend:'pdf',
                        text:'Cetak PDF',
                        title:'Laporan_'+new Date().toLocaleDateString(),
                        className:'btn btn-success btn-cetak'
                    },
                    {
                        extend:'print',
                        text:'Print',
                        title:'Laporan_'+new Date().toLocaleDateString(),
                        className:'btn btn-success btn-cetak'
                    }
                    
                ],
               
                "columns": [
                    { "data": "tanggal_ajukan" },
                    { "data": "nama_nasabah" },
                    { "data": "alamat"},
                    { "data": "jenis_mobil"},
                    { "data": "total_dp"},
                    { "data": "lama_kredit"},
                    { "data": "angsuran"},
                    { "data": "status_kredit"},
                    { "data": "status_pembayaran"}
                    ],
            });
        $('#tabel-data').show();
    } else {
        var tabel_member = $('#laporan').DataTable({
                "ajax":{ 
                        "url":"<?= base_url() ?>index.php/laporan/filter_tanggal",
                        "type": "POST",
                        "data" : {
                                    "tanggal_awal" : $('#tanggal-awal').val(),
                                    "tanggal_akhir" : $("#tanggal-akhir").val() 
                                } 
                        },
                "dataSrc":"data",
                "info":false,
                "responsive":true,
                "destroy": true,
                "searching": false,
                "lengthChange": false,
                "columns": [
                    { "data": "tanggal_ajukan" },
                    { "data": "nama_nasabah" },
                    { "data": "alamat"},
                    { "data": "jenis_mobil"},
                    { "data": "total_dp"},
                    { "data": "lama_kredit"},
                    { "data": "angsuran"},
                    { "data": "status_kredit"},
                    { "data": "status_pembayaran"}
                    ],
            });
        $('#tabel-data').show();
    }
});
    


var arr_data_member =[];
$('#datatable tbody').on('click', '.dtl-barang, .delete-barang', function() {
        $(this).addClass('selected');
        if (typeof tabel_member.row($(this).closest('tr')).data() !== 'undefined') {
            arr_data_member = tabel_member.row($(this).closest('tr')).data();
        } else if (typeof tabel_member.row(this).data() !== 'undefined') {
            arr_data_member = tabel_member.row(this).data();
        } else {
            arr_data_member = tabel_member.columns(  ).row($(this).closest('tr')).data();
        }
        if ($(this).attr('class').includes('dtl-barang')) {
          window.location.href = '/member/edit/'+arr_data_member.id;
        } else {
            swal({
              title: "Apakah Anda Yakin ingin menghapus data ini ?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                console.log(122);
                delete_member(arr_data_member.id);
              }
            });
        }
});

function delete_member(id) {
    $.ajax({
        method:"POST",
        url:"<?= base_url() ?>" + "index.php/member/delete",
        data:{ 'id' : id},
        cache:false,
        success:function(response) {
            if (response=='true') {
                window.location.href='<?= base_url() ?>'+'index.php/member';
            } else {
                alert (response);
            }
        },
        error: function(response) {
             console.log(response);
        }
    });    
}
</script>