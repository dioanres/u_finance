<!DOCTYPE html>
<html>
<head>
    <?php $this->load->helper('tanggal_indo'); ?>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Laporan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <style>
        table {
        border-collapse: collapse;
        width: 100%;
      }

      table, th, td {
          border: 1px solid black;
      }
    </style>
</head>
<body>
    <div>
        <center>
            <h3>Laporan Semua Pendapatan</h3>
        </center>
    </div>
    <table class="table" width="100%">
        <thead>
            <tr>
                <th>Tanggal Booking</th>
                <th>Nama Member</th>
                <th>Lapangan</th>
                <th>Tanggal Main</th>
                <th>Total Jam</th>
                <th>Total Harga</th>
                <th>Status Bayar</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $key => $value) {
                ?>
            <tr>
                <td><center><?php echo tanggal_lengkap($value->tanggal_booking); ?></center></td>
                <td><?php echo $value->nama_member; ?></td>
                <td><?php echo $value->nama_lapangan; ?></td>
                <td><center><?php echo tanggal_lengkap($value->tanggal_main); ?></center></td>
                <td><center><?php echo $value->total_jam; ?></center></td>
                <td><?php echo $value->total_harga; ?></td>
                <td><?php echo $value->status_bayar; ?></td>
            </tr>
            <?php 
        } ?>
        </tbody>
    </table>
</body>
</html>