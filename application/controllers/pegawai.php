<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


class pegawai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        require __DIR__.'/../../vendor/autoload.php';
        $this->load->library("session");
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {

        $this->load->view('pegawai/index');
    }

    public function tambah()
    {
        $posisi = $query = $this->db->get('posisi')->result();

        $data = array('posisi' => $posisi);
    
        $this->load->view('pegawai/form_pegawai',$data);
    }

    public function list_data()
    {
        $query = $this->db->get_where('pegawai', array('status_pegawai' => 1));
        //$query = $this->db->get('pegawai');
        $data = array('data' => $query->result());
        //dd($data);
        $this->output->set_output(json_encode($data));

    }

    public function get_data_siswa()
    {
        $query = $this->db->get('t_calon_siswa');
        $data = $query->result();

        return $data;
    }
    public function simpan()
    {

        if ($this->input->post('password') !== $this->input->post('konf_password')) {
            $this->session->set_flashdata('error', 'konfirmasi password tidak sesuai');

            redirect('/pegawai/tambah');

        }
        //dd($this->input->post());
        $data = array(
            'nik' => $this->input->post('nik'),
            'password' => md5($this->input->post('password')),
            'role' => $this->input->post('posisi'),
            'status_pegawai' => $this->input->post('status'),
            'nama' => $this->input->post('nama'),
            'alamat' => $this->input->post('alamat'),
            'email' => $this->input->post('email'),
            'no_telepon' => $this->input->post('no_telpon')
        );

        if ($this->input->post('id') && ($this->input->post('password') == '')) {
            $data = array(
                'nik' => $this->input->post('nik'),
                //'password' => md5($this->input->post('password')),
                'role' => $this->input->post('posisi'),
                'status_pegawai' => $this->input->post('status'),
                'nama' => $this->input->post('nama'),
                'alamat' => $this->input->post('alamat'),
                'email' => $this->input->post('email'),
                'no_telepon' => $this->input->post('no_telpon')
            );
            $this->db->where('id', $this->input->post('id'));
            $res = $this->db->update('pegawai', $data);
            $this->session->set_flashdata('success', 'Data Berhasil Di Ubah');
            redirect('/pegawai');
        } else if ($this->input->post('id') && ($this->input->post('password') != '')) {
            $this->db->where('id', $this->input->post('id'));
            $res = $this->db->update('pegawai', $data);
            $this->session->set_flashdata('success', 'Data Berhasil Di Ubah');
            redirect('/pegawai');
        } else {
            $res = $this->db->insert('pegawai', $data);
            $this->session->set_flashdata('success', 'Data Berhasil Ditambahkan');
            redirect('/pegawai');
        }
    }

    public function edit($id)
    {
        $query = $this->db->get_where('pegawai', array('id' => $id));
        $posisi = $this->db->get('posisi')->result();

        $data = array('posisi' => $posisi,
        'data' => $query->row());

        $this->load->view('pegawai/form_edit', $data);

    }

    public function delete()
    {

        $data = array('status_pegawai' => 0);
        $this->db->where('id', $this->input->post('id'));
            $res = $this->db->update('pegawai', $data);
        $this->output->set_output(json_encode($res));
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
