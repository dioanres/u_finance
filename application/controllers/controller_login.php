<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require __DIR__.'/../../vendor/autoload.php';

class controller_login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index()
	{
		//dd();
		$this->load->view('main/login1');
	}

	public function proses_login()
	{
		
		$this->form_validation->set_rules('username', 'Nik', 'trim|xss_clean|callback_username_check');
		$this->form_validation->set_rules('password', 'Password');
		//$this->form_validation->set_rules('as_login', 'Pilihan Login');

		if ($this->form_validation->run() == false) {
			$this->load->view('main/login1');
		} else
			if ($this->session->userdata('akses')) {
			redirect(site_url('controller_home'), 'refresh');
		} else {
			redirect(site_url('controller_home'), 'refresh');
		}

	}

	public function username_check($user)
	{
		$pass = md5($this->input->post('password'));
		
		//$as_login = $this->input->post('as_login');
		if ($user == '' && $pass == '' && $as_login == '') {
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Username dan Password Harus Diisi</div>');
			return false;
		} elseif ($user == '' || $user == null) {
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Nik harus diisi</div>');
			return false;
		} elseif ($pass == '' || $pass == null) {
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Password harus diisi</div>');
			return false;
		} else {
			$sql = "SELECT * FROM pegawai where nik='$user' and password = '$pass' and status_pegawai =1 "  ;
			$query = $this->db->query($sql);
			$data = $query->row();

			if (!empty($data)) {
				//array_push($data, 'admin');
				$this->session->set_userdata('user', $data);
				return true;
			} else {
				$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Anda Belum Terdaftar</div>');
				return false;
			}

		}
	}
	public function logout()
	{
		$this->session->unset_userdata('user');
		$this->session->sess_destroy();
		redirect(site_url('login'), 'refresh');
	}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */