<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class kredit extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        require __DIR__.'/../../vendor/autoload.php';

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('model_pegawai');
        $this->load->model('model_nasabah');

        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        $data = array("title" => "Data Kredit");
        $this->load->view('kredit/index', $data);
    }
    
    public function ajukan_kredit($id_survey)
    {
        //dd($id_survey);
        $data_survey = $this->db->get_where('survey', array('id' => $id_survey))->row();
        //dd($data_survey);
        $sales = $this->model_pegawai->get_pegawai_by_id($data_survey->id_pegawai);
        $kacab = $this->model_pegawai->get_pegawai_by_id($data_survey->id_pegawai_kacab);
        $mobil = $this->model_nasabah->get_mobil_by_id($data_survey->id_mobil);
        //dd($sales);
        $data = array(
                    'status_kredit' => $this->status_kredit(),
                    'data_survey' => $data_survey,
                    'sales' => $sales,
                    'kacab' => $kacab,
                    'mobil' => $mobil
                );
        //dd($data);
        $this->load->view('kredit/form_kredit',$data);
    }

    public function simpan_kredit()
    {
        $data = array(
            'total_dp' => $this->input->post('total_dp'),
            'angsuran' => $this->input->post('angsuran'),
            'lama_kredit' => $this->input->post('lama_kredit'),
            'status_pembayaran' => $this->input->post('status_kredit'),
            'id_survey' => $this->input->post('id_survey') 
        );

        $svy = array(
            'status_kredit' => 1
        );
        
        if ($this->input->post('id_kredit')) {
            $this->db->where('id', $this->input->post('id_kredit'));
            $res = $this->db->update('kredit', $data);

            $this->session->set_flashdata('success', 'Data Berhasil Di Ubah');
            redirect('/kredit');
        } else {
            $res = $this->db->insert('kredit', $data);

            //update table survey
            $this->db->update('survey', $svy, array('id' => $data['id_survey']));
            $this->session->set_flashdata('success', 'Data Berhasil Ditambahkan');
            redirect('/kredit');
        }


    }

    public function list_data()
    {
        $sql = "SELECT a.id,c.nama nama_nasabah, e.jenis_mobil, a.total_dp, a.angsuran, a.lama_kredit, a.tanggal_ajukan 
        FROM kredit a 
        join survey b on a.id_survey = b.id
        join nasabah c on b.id_nasabah = c.id
        join pegawai d on b.id_pegawai = d.id
        join mobil e on b.id_mobil = e.id";
        $query = $this->db->query($sql);
        $data = array('data' => $query->result());
        
        $this->output->set_output(json_encode($data));
    }

    public function list_verifikasi()
    {
        $sql = "SELECT a.id as id_survey ,a.status_survey,a.status_kredit,b.nama as nama_sales, c.nama as nama_nasabah , c.*
        FROM survey a
        JOIN pegawai b on a.id_pegawai = b.id
        JOIN nasabah c on a.id_nasabah = c.id
        JOIN kredit d on a.id = d.id_survey
        WHERE a.status_survey = 1
        ";

        $query = $this->db->query($sql);
        $data = array('data' => $query->result());
        
        $this->output->set_output(json_encode($data));
    }

    public function edit($id){

        $data_kredit = $this->db->get_where('kredit', array('id' => $id))->row();

        $data_survey = $this->db->get_where('survey', array('id' => $data_kredit->id_survey))->row();
        
        $sales = $this->model_pegawai->get_pegawai_by_id($data_survey->id_pegawai);
        $kacab = $this->model_pegawai->get_pegawai_by_id($data_survey->id_pegawai_kacab);
        $mobil = $this->model_nasabah->get_mobil_by_id($data_survey->id_mobil);
        //dd($sales);
        $data = array(
                    'data_kredit' => $data_kredit,
                    'id_kredit' => $id,
                    'status_kredit' => $this->status_kredit(),
                    'data_survey' => $data_survey,
                    'sales' => $sales,
                    'kacab' => $kacab,
                    'mobil' => $mobil
                );

        $this->load->view('kredit/form_kredit',$data);
    }
    public function delete()
    {
        if ($this->session->userdata('user')->role == '2') {
            $res = 'Anda Tidak Memiliki Akses !';
        } else {
            $res = $this->db->delete('kredit', array('id' => $this->input->post('id')));
        }
        $this->output->set_output(json_encode($res));
    }

    public function status_kredit()
    {
        $status  = array('0' => 'Belum Lunas' , '1' => 'Lunas' );

        return $status;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */