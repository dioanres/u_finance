<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class mobil extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		require __DIR__.'/../../vendor/autoload.php';

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		date_default_timezone_set("Asia/Jakarta");
	}

	protected $stok;

	public function index()
	{
		$data = array("title" => "Data Mobil");
		$this->load->view('mobil/index', $data);
	}

	public function tambah()
	{
		$stok = ['0' => 'Kosong', '1' => 'Ready', '2' => 'Indent'];

		$data = array('stok' => $stok);
		$this->load->view('mobil/tambah',$data);
	}

	public function edit($id)
	{

		$query = $this->db->get_where('mobil', array('id' => $id));
		$stok = ['0' => 'Kosong', '1' => 'Ready', '2' => 'Indent'];

		$data['data'] = $query->row();
		$data['stok'] = $stok;
		$this->load->view('mobil/edit', $data);
	}

	public function list_data()
	{
		$query = $this->db->get('mobil');
		$data = array('data' => $query->result());
		$this->output->set_output(json_encode($data));
	}

	public function simpan()
	{

		$this->form_validation->set_rules('jenis_mobil', 'Jenis Mobil', 'required');
		$this->form_validation->set_rules('harga_mobil', 'Harga Mobil', 'required');
		$this->form_validation->set_rules('ketersediaan', 'Stok Mobil', 'required');

		$stok = ['0' => 'Kosong', '1' => 'Ready', '2' => 'Indent'];

		$data = array('stok' => $stok);
		//dd($this->input->post());
		if ($this->form_validation->run() == false) {
			if ($this->input->post('id_barang')) {
				$this->load->view('mobil/edit');
			} else {
				$this->load->view('mobil/tambah',$data);
			}
		} else {
			$data = array(
				'jenis_mobil' => $this->input->post('jenis_mobil'),
				'warna_mobil' => $this->input->post('warna_mobil'),
				'harga_mobil' => $this->input->post('harga_mobil'),
				'bahan_bakar' => $this->input->post('bahan_bakar'),
				'cc_mobil' => $this->input->post('cc_mobil'),
				'no_rangka' => $this->input->post('no_rangka'),
				'no_mesin' => $this->input->post('no_mesin'),
				'ketersediaan' => $this->input->post('ketersediaan')
			);

			if ($this->input->post('id')) {
				$this->db->where('id', $this->input->post('id'));
				$res = $this->db->update('mobil', $data);
				$this->session->set_flashdata('success', 'Data Berhasil Di Ubah');
				redirect('/mobil');
			} else {
				$res = $this->db->insert('mobil', $data);
				$this->session->set_flashdata('success', 'Data Berhasil Ditambahkan');
				redirect('/mobil');
			}

		}

	}

	public function delete()
	{
		$res = $this->db->delete('mobil', array('id' => $this->input->post('id')));
		$this->output->set_output(json_encode($res));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */