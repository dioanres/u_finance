<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class absensi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		require __DIR__.'/../../vendor/autoload.php';

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('model_pegawai');
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index()
	{
		$data = array("title" => "Data Absensi");
		$this->load->view('absensi/index', $data);
	}

	public function tambah()
	{
		$query = $this->db->get('pegawai');
		$data = array('data' => $query->result());
		
		$this->load->view('absensi/form_absen',$data);
	}

	public function edit($id)
	{
		$query = $this->db->get_where('absen', array('id' => $id));
		$pegawai = $this->db->get('pegawai')->result();
		$data['data'] = $query->row();
		$data['pegawai'] = $pegawai;
		
		$this->load->view('absensi/edit', $data);
	}

	public function list_data()
	{
		//$query = $this->db->get('absen');
		$sql = 'SELECT a.*, b.nama, b.nik FROM absen a
		JOIN pegawai b on a.id_pegawai = b.id';
		$query = $this->db->query($sql)->result();
		//dd($query);
		$data = array('data' => $query);
		$this->output->set_output(json_encode($data));
		/*print_r(json_encode($data));
		exit();*/
	}

	public function simpan()
	{

		$this->form_validation->set_rules('tanggal', 'Tanggal Masuk', 'required');
		$this->form_validation->set_rules('absen_masuk', 'Absen Masuk', 'required');
		$this->form_validation->set_rules('absen_pulang', 'Absen Pulang', 'required');
		$this->form_validation->set_rules('nik_pegawai', 'Nik Pegawai', 'required');

		if ($this->form_validation->run() == false) {
			if ($this->input->post('id')) {
				$this->load->view('absensi/edit');
			} else {
				$this->load->view('absensi/form_lapangan');
			}
		} else {
			$data = array(
				'tanggal' => date('Y-m-d',strtotime($this->input->post('tanggal'))) ,
				'absen_msk' => $this->input->post('absen_masuk'),
				'absen_plg' => $this->input->post('absen_pulang'),
				'id_pegawai' => $this->input->post('nik_pegawai')
			);

			if ($this->input->post('id')) {
				$this->db->where('id', $this->input->post('id'));
				$res = $this->db->update('absen', $data);
				$this->session->set_flashdata('success', 'Data Berhasil Di Ubah');
				redirect('/absensi');
			} else {
				$res = $this->db->insert('absen', $data);
				$this->session->set_flashdata('success', 'Data Berhasil Ditambahkan');
				redirect('/absensi');
			}

		}

	}

	public function delete()
	{
		//dd($this->input->post('id'));
		if ($this->session->userdata('user')->role == '2') {
			$res = 'Anda Tidak Memiliki Akses !';
		} else {
			$res = $this->db->delete('absen', array('id' => $this->input->post('id')));
		}
		$this->output->set_output(json_encode($res));
	}

	public function absen(){
		$user = $this->session->userdata('user');
		$date = date('Y-m-d');


		$cek_absen = $this->db->get_where('absen',array('id_pegawai' => $user->id, 'tanggal' => $date))->row();
         //dd(empty($cek_absen));
		if(!empty($cek_absen)){
			$data['data'] = '<div>
			<span>Anda sudah melakukan absen masuk hari ini </span>
		</div>
		<img src=" '.base_url().'assets/img/icon/icon_hadir.png" style="height:130px;">';
		} else {
			$data['data'] = '<button type="button" class="btn btn-round btn-success" id="btn-hadir" style="width:100%;">Absen</button>';
		}

		$this->load->view('absensi/absen',$data);
	}

	public function absen_pulang() {
		$user = $this->session->userdata('user');
		$date = date('Y-m-d');

		$cek_absen = $this->db->get_where('absen',array('id_pegawai' => $user->id, 'tanggal' => $date))->row();

		if($cek_absen->absen_plg == null) {
			$data['data'] = '<button type="button" class="btn btn-round btn-success" id="btn-pulang" style="width:100%;">Absen</button>';
		} else {
			$data['data'] = '<div>
			<span>Anda sudah melakukan absen pulang hari ini </span>
		</div>
		<img src=" '.base_url().'assets/img/icon/icon_hadir.png" style="height:130px;">';
		}
		$this->load->view('absensi/absen',$data);
	}

	public function absen_pegawai(){
		$user = $this->session->userdata('user');
		
		$data = array(
			'tanggal' => date('Y-m-d') ,
			'absen_msk' => date('h:i:s'),
			'id_pegawai' => $user->id
		);

		$res = $this->db->insert('absen', $data);

		$this->output->set_output(json_encode($res));
		//dd($data);
	}

	public function proses_absen_pulang(){
		$user = $this->session->userdata('user');
		$date = date('Y-m-d');
		$cek_absen = $this->db->get_where('absen',array('id_pegawai' => $user->id, 'tanggal' => $date))->row();
		//dd($cek_absen);
		$data = array(
			'absen_plg' => date('h:i:s')
		);

		$this->db->where('id', $cek_absen->id);
		$res = $this->db->update('absen', $data);

		$this->output->set_output(json_encode($res));
		//dd($data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */