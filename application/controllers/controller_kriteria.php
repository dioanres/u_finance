<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class controller_kriteria extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('model_siswa');
        date_default_timezone_set("Asia/Jakarta");
    }

	public function index()
	{

		$this->load->view('form/index_kategori');
	}
	
	public function tambah(){
		$bobot['listbobot'] = $this->list_bobot();
		 
		$this->load->view('form/form_kriteria',$bobot);
	}

	public function list_data() {
		$query = $this->db->get('t_kriteria');
		$data = array('data' => $query->result());
		$this->output->set_output(json_encode($data));

	}
	public function simpan() {
		/*var_dump($this->input->post());
		exit();*/
		$data = array(
		        'nama_kriteria' => $this->input->post('nama_kriteria'),
		        'kategori' => $this->input->post('kategori'),
		        'nilai_bobot' => $this->input->post('nilai_bobot')
		);

		if ($this->input->post('id')) {
			$this->db->where('id_kriteria',$this->input->post('id'));
			$res=$this->db->update('t_kriteria', $data);
			$this->output->set_output(json_encode($res));
		} else {
			$res=$this->db->insert('t_kriteria', $data);
			//$res=$this->model_siswa->simpan($data);
			$this->output->set_output(json_encode($res));
		}
	}

	public function delete() {
		
		$res = $this->db->delete('t_kriteria', array('id_kriteria' => $this->input->post('id')));
		$this->output->set_output(json_encode($res));
	}

	public function list_bobot() {
		$query = $this->db->get('t_bobot');
		$data = $query->result();//array('data' => $query->result());
		return $data ;
		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */