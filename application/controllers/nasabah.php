<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class nasabah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        
        require __DIR__.'/../../vendor/autoload.php';

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->load->model('model_nasabah');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        $data = array("title" => "Data Nasabah");
        $this->load->view('nasabah/index', $data);
    }

    public function tambah()
    {
        $data = array("title" => "Data Nasabah");
        $this->load->view('nasabah/tambah', $data);
    }

    public function edit($id)
    {

        $query = $this->db->get_where('nasabah', array('id' => $id));

        $data = array(
            'data' => $query->row(),
            'title' => 'Ubah Data Nasabah'
        );
        $this->load->view('nasabah/edit', $data);
    }

    public function list_data()
    {
        $nasabah = $this->model_nasabah->get_data_nasabah();
        $data = array('data' => $nasabah);
        $this->output->set_output(json_encode($data));
    }

    public function simpan()
    {

        $this->form_validation->set_rules('nama', 'Nama Member', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('no_hp', 'No HP', 'required');

        if ($this->form_validation->run() == false) {
            if ($this->input->post('id')) {
                $this->load->view('nasabah/edit');
            } else {
                //redirect('/nasabah/tambah');
                $data = array("title" => "Data Nasabah");
                $this->load->view('nasabah/tambah',$data);
            }
        } else {
            $data = array(
                'nama' => $this->input->post('nama'),
                'alamat' => $this->input->post('alamat'),
                'no_telpon' => $this->input->post('no_hp'),
                'no_ktp' => $this->input->post('no_ktp'),
                'no_pbb' => $this->input->post('no_pbb'),
                'rek_koran' => $this->input->post('rek_koran'),
                'npwp' => $this->input->post('no_npwp'),
                'slip_gaji' => $this->input->post('slip_gaji'),
                'sku' => $this->input->post('sku')
            );
            
            if ($this->input->post('id')) {
                $this->db->where('id', $this->input->post('id'));
                $res = $this->db->update('nasabah', $data);
                $this->session->set_flashdata('success', 'Data Berhasil Di Ubah');
                redirect('/nasabah');
            } else {
                $res = $this->db->insert('nasabah', $data);
                $this->session->set_flashdata('success', 'Data Berhasil Ditambahkan');
                redirect('/nasabah');
            }

        }

    }

    public function delete()
    {
        if ($this->session->userdata('user')->role == '2') {
            $res = 'Anda Tidak Memiliki Akses !';
        } else {
            $res = $this->db->delete('nasabah', array('id' => $this->input->post('id')));
        }
        $this->output->set_output(json_encode($res));
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */