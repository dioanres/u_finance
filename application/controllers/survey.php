<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class survey extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        require __DIR__ . '/../../vendor/autoload.php';

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('model_pegawai');
        $this->load->model('model_nasabah');

        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        $data = array("title" => "Data Survey");
        $this->load->view('survey/index', $data);
    }

    public function tambah()
    {
        $sales = $this->model_pegawai->get_data_sales();
        $nasabah = $this->model_nasabah->get_data_nasabah();
        $status = $this->status_survey();
        $mobil = $this->db->get('mobil')->result();

        $data = array(
            "title" => "Data Posisi Jabatan",
            'sales' => $sales,
            'nasabah' => $nasabah,
            'status' => $status,
            'mobil' => $mobil,
        );
        //dd($data);
        $this->load->view('survey/tambah', $data);
    }

    public function edit($id)
    {
        $data = $this->db->get_where('survey', array('id' => $id))->row();
        $sales = $this->model_pegawai->get_data_sales();
        $nasabah = $this->model_nasabah->get_data_nasabah();
        $mobil = $this->db->get('mobil')->result();
        $status = $this->status_survey();

        $data = array(
            'title' => 'Ubah Data Posisi',
            'data' => $data,
            'sales' => $sales,
            'nasabah' => $nasabah,
            'status' => $status,
            'mobil' => $mobil,
        );

        $this->load->view('survey/edit', $data);
    }

    public function list_data()
    {
        $sql = "SELECT a.id as id_survey ,a.status_survey,a.status_kredit,b.nama as nama_sales, c.nama as nama_nasabah , c.*
        FROM survey a
        JOIN pegawai b on a.id_pegawai = b.id
        JOIN nasabah c on a.id_nasabah = c.id";

        $query = $this->db->query($sql);
        $data = array('data' => $query->result());
        //dd($data);
        $this->output->set_output(json_encode($data));
    }

    public function detail($id)
    {
        $sql = "SELECT a.id as id_survey ,a.status_survey,a.status_kredit,b.nama as nama_sales, c.nama as nama_nasabah ,c.*,d.*
        FROM survey a
        JOIN pegawai b on a.id_pegawai = b.id
        JOIN nasabah c on a.id_nasabah = c.id
        left JOIN kredit d on a.id = d.id_survey
        WHERE a.id = $id";
        $query = $this->db->query($sql)->row();
        //dd($sql,$query);
        $sales = $this->model_pegawai->get_data_sales();
        $nasabah = $this->model_nasabah->get_data_nasabah();
        $status = $this->status_verifikasi();

        $data = array(
            'title' => 'Ubah Data Posisi',
            'data' => $query,
            'sales' => $sales,
            'nasabah' => $nasabah,
            'status' => $status,
        );
        //dd($data);
        $this->load->view('survey/detail', $data);
    }
    public function simpan()
    {

        $this->form_validation->set_rules('sales', 'Nama Sales', 'required');
        $this->form_validation->set_rules('nasabah', 'Nama Nasabah', 'required');

        if ($this->form_validation->run() == false) {
            if ($this->input->post('id')) {
                $this->load->view('survey/edit');
            } else {
                $this->load->view('survey/tambah');
            }
        } else {
            $data = array(
                'id_pegawai' => $this->input->post('sales'),
                'id_nasabah' => $this->input->post('nasabah'),
                'id_mobil' => $this->input->post('mobil'),
                'created_at' => date('Y-m-d h:i:s'),
                'status_survey' => $this->input->post('status'),
            );
            //dd($data,$this->input->post());
            if ($this->input->post('id')) {
                $this->db->where('id', $this->input->post('id'));
                $res = $this->db->update('survey', $data);
                $this->session->set_flashdata('success', 'Data Berhasil Di Ubah');
                redirect('/survey');
            } else {
                $res = $this->db->insert('survey', $data);
                $this->session->set_flashdata('success', 'Data Berhasil Ditambahkan');
                redirect('/survey');
            }

        }

    }

    public function delete()
    {
        if ($this->session->userdata('user')->role == '2') {
            $res = 'Anda Tidak Memiliki Akses !';
        } else {
            $res = $this->db->delete('survey', array('id' => $this->input->post('id')));
        }
        $this->output->set_output(json_encode($res));
    }

    public function status_survey()
    {
        $status = array('0' => 'Belum diproses', '1' => 'Sudah Disurvey');

        return $status;
    }

    public function status_kredit()
    {
        $status = array('0' => 'Belum Lunas', '1' => 'Lunas');

        return $status;
    }

    public function status_verifikasi()
    {
        $status = array('1' => 'Belum Diverifikasi', '2' => 'Diterima', '-1' => 'Ditolak');
        return $status;
    }

    public function verifikasi()
    {

        $data = array(
            'status_kredit' => $this->input->post('status'),
            'id_pegawai_kacab' => $this->session->userdata('user')->id,
        );
        //dd($data);
        //dd($data,$this->input->post('status') == 1);
        $this->db->where('id', $this->input->post('id'));
        $res = $this->db->update('survey', $data);
        //dd($data,$res,$this->input->post('id'));
        $this->session->set_flashdata('success', 'Data Berhasil Di Verifikasi');

        redirect('/survey');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
