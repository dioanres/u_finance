<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class laporan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helpers('dompdf');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        $data = array("title" => "Laporan Data Kredit");
        $this->load->view('laporan/index', $data);
    }

    public function tampil_semua()
    {
        $sql = "SELECT e.nama as nama_nasabah, e.alamat,d.jenis_mobil ,DATE_FORMAT(a.tanggal_ajukan, '%d-%m-%Y') tanggal_ajukan,a.total_dp,a.lama_kredit, a.angsuran,
        CASE WHEN a.status_pembayaran = 0 THEN 'Belum Lunas' ELSE 'Lunas' END status_pembayaran, 
        CASE WHEN b.status_kredit = 2 THEN 'Diterima' WHEN b.status_kredit = -1 THEN 'Ditolak' END status_kredit
        FROM kredit a
        JOIN survey b on a.id_survey = b.id
        JOIN pegawai c on b.id_pegawai = c.id
        JOIN mobil d on b.id_mobil = d.id
        JOIN nasabah e on b.id_nasabah = e.id
        WHERE b.status_survey = 1";

        $query = $this->db->query($sql);
        $data = array('data' => $query->result());
        $this->output->set_output(json_encode($data));
    }

    public function filter_tanggal()
    {
        $tanggal_awal = date('Y-m-d', strtotime($this->input->post('tanggal_awal')));
        $tanggal_akhir = date('Y-m-d', strtotime($this->input->post('tanggal_akhir')));
        $sql = "
        SELECT e.nama as nama_nasabah, e.alamat,d.jenis_mobil  ,DATE_FORMAT(a.tanggal_ajukan, '%d-%m-%Y') tanggal_ajukan ,a.total_dp,a.lama_kredit, a.angsuran,
        CASE WHEN a.status_pembayaran = 0 THEN 'Belum Lunas' ELSE 'Lunas' END status_pembayaran, 
        CASE WHEN b.status_kredit = 2 THEN 'Diterima' WHEN b.status_kredit = -1 THEN 'Ditolak' END status_kredit
        FROM kredit a
        JOIN survey b on a.id_survey = b.id
        JOIN pegawai c on b.id_pegawai = c.id
        JOIN mobil d on b.id_mobil = d.id
        JOIN nasabah e on b.id_nasabah = e.id
        WHERE b.status_survey = 1
        and DATE_FORMAT(a.tanggal_ajukan, '%Y-%m-%d')
        BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' ;
        ";

        $query = $this->db->query($sql);
        $data = array('data' => $query->result());
        $this->output->set_output(json_encode($data));
    }

    public function download_semua()
    {
        $sql = "SELECT a.tanggal_booking,
                    c.nama_member,
                    b.nama_lapangan, 
                    a.tanggal_main,
                    (hour(a.jam_habis)- hour(a.jam_mulai)) total_jam,
                    a.total_harga,
                    case 
                    when (a.status_bayar = 0)
                    then 'Belum Bayar'
                    when (a.status_bayar = 1)
                    then 'DP'
                    when (a.status_bayar = 2)
                    then 'Lunas'
                    end as status_bayar
            FROM data_transaksi a
            join data_lapangan b on a.id_lapangan = b.id_lapangan
            join data_member c on a.id_member = c.id";

        $query = $this->db->query($sql);
        $data = array('data' => $query->result());

        $html = $this->load->view('laporan/download', $data, true);
        $filename = 'Laporan ' . date('d-m-Y');
        $paper = 'A4';
        $orientation = 'portrait';

        pdf_create($html, $filename, $paper, $orientation);
    }
    public function simpan()
    {

        $this->form_validation->set_rules('nama_member', 'Nama Member', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('no_hp', 'No HP', 'required');

        if ($this->form_validation->run() == false) {
            if ($this->input->post('id')) {
                $this->load->view('member/edit');
            } else {
                $this->load->view('member/tambah');
            }
        } else {
            $data = array(
                'nama_member' => $this->input->post('nama_member'),
                'alamat' => $this->input->post('alamat'),
                'no_hp' => $this->input->post('no_hp')
            );

            if ($this->input->post('id')) {
                $this->db->where('id', $this->input->post('id'));
                $res = $this->db->update('data_member', $data);
                $this->session->set_flashdata('success', 'Data Berhasil Di Ubah');
                redirect('/member');
            } else {
                $res = $this->db->insert('data_member', $data);
                $this->session->set_flashdata('success', 'Data Berhasil Ditambahkan');
                redirect('/member');
            }

        }

    }

    public function delete()
    {
        $res = $this->db->delete('data_member', array('id' => $this->input->post('id')));
        $this->output->set_output(json_encode($res));
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */