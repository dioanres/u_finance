<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class posisi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        require __DIR__.'/../../vendor/autoload.php';

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        $data = array("title" => "Data Posisi Jabatan");
        $this->load->view('posisi/index', $data);
    }

    public function tambah()
    {
        $data = array(
            "title" => "Data Posisi Jabatan",
        );
        $this->load->view('posisi/tambah', $data);
    }

    public function edit($id)
    {
        $data = $this->db->get_where('posisi', array('id' => $id))->row();

        $data = array(
            'title' => 'Ubah Data Posisi',
            'data' => $data
        );
        $this->load->view('posisi/edit', $data);
    }

    public function list_data()
    {
        $query = $this->db->get('posisi');
        $data = array('data' => $query->result());
        $this->output->set_output(json_encode($data));
    }

    public function simpan()
    {

        $this->form_validation->set_rules('nama_posisi', 'Nama Posisi', 'required');

        if ($this->form_validation->run() == false) {
            if ($this->input->post('id')) {
                $this->load->view('posisi/edit');
            } else {
                $this->load->view('posisi/tambah');
            }
        } else {
            $data = array(
                'nama_posisi' => $this->input->post('nama_posisi')
            );
            if ($this->input->post('id')) {
                $this->db->where('id', $this->input->post('id'));
                $res = $this->db->update('posisi', $data);
                $this->session->set_flashdata('success', 'Data Berhasil Di Ubah');
                redirect('/posisi');
            } else {
                $res = $this->db->insert('posisi', $data);
                $this->session->set_flashdata('success', 'Data Berhasil Ditambahkan');
                redirect('/posisi');
            }

        }

    }

    public function delete()
    {
        if ($this->session->userdata('user')->role == '2') {
            $res = 'Anda Tidak Memiliki Akses !';
        } else {
            $res = $this->db->delete('posisi', array('id' => $this->input->post('id')));
        }
        $this->output->set_output(json_encode($res));
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */